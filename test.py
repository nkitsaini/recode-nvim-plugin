import pynvim
import pathlib
import time
import recode
import dacite
import json
import re

# this is pretty low we may need to increase this. it takes 0.003 s
WAIT_TIME = 0.005  # let the plugin respond to autocmd. is there better way?

def test_editor_write(vim: pynvim.Nvim):
	assert len(vim.current.buffer) == 1
	assert vim.current.buffer[0] == ""
	vim.current.buffer[0] = "hiii"
	assert vim.current.line == "hiii"


def test_start_stop(vim: pynvim.Nvim, tmp_path: pathlib.Path):
	file_path = tmp_path/"record.rd.jsonl"
	vim.command("UpdateRemotePlugins")
	vim.command(f"call StartRecording('{file_path.as_posix()}')")
	vim.command("call StopRecording()")
	assert open(file_path).read() == ""


def replace_buffer(vim: pynvim.Nvim, new_content: str):
	with open("/tmp/recodvim", "w") as f:
		f.write(new_content)
	vim.command(f'exe "%!cat /tmp/recodvim"')


def replace_buffer_wait(vim: pynvim.Nvim, new_content: str):
	replace_buffer(vim, new_content)
	time.sleep(WAIT_TIME)


def test_start_write_stop(vim: pynvim.Nvim, tmp_path: pathlib.Path):
	file_path = tmp_path/"record.rd.jsonl"
	vim.command(f"call StartRecording('{file_path.as_posix()}')")
	replace_buffer_wait(vim, "hello")

	vim.command("call StopRecording()")
	assert "hello" in open(file_path).read()
	assert len(re.findall("hello", open(file_path).read())) == 1


def test_start_many_writes_stop(vim: pynvim.Nvim, tmp_path: pathlib.Path):
	file_path = tmp_path/"record.rd.jsonl"
	vim.command(f"call StartRecording('{file_path.as_posix()}')")
	states = [
		"hello",
		"hello\nbye",
		"hello\n\nbye",
		"hello\nab\nbye",
	]
	for s in states:
		replace_buffer_wait(vim, s)

	vim.command("call StopRecording()")
	with open(file_path) as f:
		lines = f.readlines()
	snapshots = [dacite.from_dict(recode.models.SnapShot, json.loads(l)) for l in lines]
	last = ""
	i = 0
	for ss in snapshots:
		curr = ss.state.buffer_change.apply(last)
		if last != curr:
			assert curr == states[i], (i, len(snapshots), list(map(lambda x: x.state.buffer_change, snapshots)))
			i+=1
		last = curr
