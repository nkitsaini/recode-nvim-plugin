import json
import pytest
import pynvim

pynvim.setup_logging("test")
child_argv = '["nvim", "-u", "./vimrc", "--embed", "--headless"]'

@pytest.fixture(scope="session")
def plugged_vim():
	editor = pynvim.attach('child', argv=json.loads(child_argv))

	# reload plugin
	editor.command("UpdateRemotePlugins")
	editor.close()
	editor = pynvim.attach('child', argv=json.loads(child_argv))
	return editor


@pytest.fixture
def vim(plugged_vim: pynvim.Nvim):

	plugged_vim.command("norm ggdG")
	# editor = pynvim.attach('socket',path="/tmp/nvim")
	return plugged_vim
