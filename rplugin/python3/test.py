import pynvim
import recode
import arrow
from pathlib import Path
import recode
import recode.models as m
import structlog
import time

import typing as t
from dataclasses import asdict
import json
import dacite
from typing import cast
import jsonlines


structlog.configure(
	logger_factory=structlog.PrintLoggerFactory(file=open("/home/ankit/logs/recodvim.log", "a"))
)

DEFAULT_DIR = Path("/home/ankit/recode")

@pynvim.plugin
class Main:
	def __init__(self, vim: pynvim.Nvim):
		self.vim = vim
		self.last_buffer = ""
		self.start_timestamp = None
		self.f = None
		self.file_path = None
		self.log = structlog.get_logger()
		self.log.msg("Loaded Plugin")
		self.last_snapshot__ms = self.get_timestamp()

	@classmethod
	def get_timestamp(cls):
		return int(arrow.now().float_timestamp * 1000)

	def get_line(self) -> int:
		return int(self.vim.call("line", ".")) # type: ignore

	def get_char(self) -> int:
		return int(self.vim.call("col", ".")) # type: ignore

	def get_buffer(self) -> str:
		return '\n'.join(self.vim.current.buffer)

	@pynvim.function('RecodTestFunction')
	def test_function(self, args):
		self.log.msg("Test function")
		msg = """first
		second
		third"""
		self.replace_buffer(msg)

	@pynvim.function('StartRecording')
	def start_recording(self, args):
		if len(args) > 1:
			self.vim.command(f'echo "expected only 1 or 0 args" found {args}')
			return
		elif len(args) == 1:
			self.file_path = Path(args[0])
		else:
			self.file_path = DEFAULT_DIR/(arrow.now().isoformat()+'.rd.jsonl')
		self.f = open(self.file_path, "w")
		self.start_timestamp = self.get_timestamp()
		self.last_buffer = ""
		self.last_snapshot__ms = self.get_timestamp()

		self.log.msg("started_recording", file = self.file_path)

	def take_ss(self):
		if self.f is None:
			return
		current_ss = self.get_current_snapshot()
		with jsonlines.Writer(self.f) as jsonf:
			jsonf.write(asdict(current_ss))
		self.last_snapshot__ms = self.get_timestamp()
		self.log.msg("taking_snapshot", snapshot= current_ss)

	@pynvim.autocmd("TextChangedP", sync=True)
	def take_snapshot0(self):
		self.log.msg("textchangedP")
		self.take_ss()

	@pynvim.autocmd("TextChangedI", sync=True)
	def take_snapshot1(self):
		self.log.msg("textchangedI")
		self.take_ss()

	@pynvim.autocmd("TextChanged", sync=True)
	def take_snapshot2(self):
		self.log.msg("text changed")
		self.take_ss()

	@pynvim.autocmd("CursorMoved", sync=True)
	def take_snapshot3(self):
		self.log.msg("cursormoved")
		# if self.last_snapshot__ms > self.get_timestamp() - 10:
		# 	self.log.msg("dropping", last = self.last_snapshot__ms, timestamp=self.get_timestamp())
		# 	return
		self.take_ss()

	@pynvim.autocmd("CursorMovedI", sync=True)
	def take_snapshot4(self):
		self.log.msg("cursormovedI")
		# if self.last_snapshot__ms > self.get_timestamp() - 10:
		# 	self.log.msg("dropping", last = self.last_snapshot__ms, timestamp=self.get_timestamp())
		# 	return
		self.take_ss()

	@pynvim.function("StopRecording")
	def stop_recording(self, args):
		self.log.msg("Stopping Recording", file_path = self.file_path)
		self.f = None
		self.file_path = None

	def get_current_snapshot(self) -> t.Optional[m.SnapShot]:
		if self.f is None:
			return
		assert self.start_timestamp is not None
		relative_timestamp = self.get_timestamp() - self.start_timestamp
		curr_buffer = self.get_buffer()

		buffer_change = m.BufferChange.from_buffers(self.last_buffer, curr_buffer)
		curosr_position = m.Position(self.get_line(), self.get_char())

		snapshot = m.SnapShot(relative_timestamp, m.EditorState(curosr_position, buffer_change, None))

		self.last_buffer = curr_buffer
		return snapshot

	# todo send user_msg
	def send_error(self, msg):
		self.vim.command(f'echo "{msg}"')

	def replace_buffer(self, new_content: str):
		with open("/tmp/recodvim", "w") as f:
			f.write(new_content)
		self.vim.command(f'exe "%!cat /tmp/recodvim"')

	def move_cursor(self, line: int, char: int):
		self.vim.command(f'norm {line}G{char-1}l"')

	@pynvim.function('StartPlaying')
	def start_playing(self, args):
		self.vim.command("norm ggdG")
		self.log.msg("starting play")
		file_path: Path;
		if len(args) > 1:
			self.vim.command(f'echo "expected only 1 or 0 args" found {args}')
			return
		elif len(args) == 1:
			file_path = Path(args[0])
		else:
			latest = None
			for fil in DEFAULT_DIR.iterdir():
				if not fil.is_file():
					continue
				if latest is None or latest < fil.as_posix():
					latest = fil.as_posix()
			if latest is None:
				self.send_error(f"No file to play")
				return
			file_path = Path(latest)

		if not file_path.is_file():
			self.send_error(f"file {file_path.as_posix()} not found")
			return

		self.log.msg("starting rendering play", file_path=file_path)
		with open(file_path, "r") as f:

			start_timestamp = self.get_timestamp()
			curr_buffer = self.get_buffer()

			for ss in f.readlines():
				snapshot = dacite.from_dict(data_class=m.SnapShot, data=json.loads(ss))

				sleep_time = max((start_timestamp + snapshot.timestamp__ms) - self.get_timestamp(), 0)/1000
				self.log.msg("sleeping", sleep_time = sleep_time)
				time.sleep(sleep_time)
				curr_buffer = snapshot.state.buffer_change.apply(curr_buffer)

				self.replace_buffer(curr_buffer)
				self.move_cursor(snapshot.state.cursor_position.line, snapshot.state.cursor_position.char)

		self.log.msg("ending play", file = file_path, total_lines_at_end=len(curr_buffer.split("\n")))
		self.send_error("Playing done")

